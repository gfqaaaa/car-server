DROP TABLE IF EXISTS pms_album;
CREATE TABLE pms_album
(
    id           bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '数据id',
    name         varchar(50)         DEFAULT NULL COMMENT '图片名称',
    description  varchar(255)        DEFAULT NULL COMMENT '图片简介',
    sort         tinyint(3) unsigned DEFAULT NULL COMMENT '排序序号',
    gmt_create   datetime            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified datetime            DEFAULT NULL COMMENT '数据最后修改时间',
    PRIMARY KEY (id)
) DEFAULT CHARSET = utf8mb4 COMMENT ='图片';


DROP TABLE IF EXISTS pms_attribute;
CREATE TABLE pms_attribute
(
    id                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '数据id',
    template_id        bigint(20) unsigned DEFAULT NULL COMMENT '所属属性模版id',
    name               varchar(50)         DEFAULT NULL COMMENT '属性名称',
    description        varchar(255)        DEFAULT NULL COMMENT '属性简介（某些属性名称可能相同，通过简介补充描述）',
    notes              varchar(255)        DEFAULT NULL COMMENT '备注',
    sort               tinyint(3) unsigned DEFAULT NULL COMMENT '排序序号',
    gmt_create         datetime            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified       datetime            DEFAULT NULL COMMENT '数据最后修改时间',
    PRIMARY KEY (id)
) DEFAULT CHARSET = utf8mb4 COMMENT ='属性';



DROP TABLE IF EXISTS pms_attribute_template;
CREATE TABLE pms_attribute_template
(
    id           bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '数据id',
    name         varchar(50)         DEFAULT NULL COMMENT '属性模版名称',
    pinyin       varchar(50)         DEFAULT NULL COMMENT '属性模版名称的拼音',
    keywords     varchar(255)        DEFAULT NULL COMMENT '关键词列表，各关键词使用英文的逗号分隔',
    sort         tinyint(3) unsigned DEFAULT NULL COMMENT '排序序号',
    gmt_create   datetime            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified datetime            DEFAULT NULL COMMENT '数据最后修改时间',
    PRIMARY KEY (id)
) DEFAULT CHARSET = utf8mb4 COMMENT ='属性模版';


DROP TABLE IF EXISTS pms_brand;
CREATE TABLE pms_brand
(
    id                     bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '数据id',
    name                   varchar(50)         DEFAULT NULL COMMENT '品牌名称',
    pinyin                 varchar(255)        DEFAULT NULL COMMENT '品牌名称的拼音',
    description            varchar(255)        DEFAULT NULL COMMENT '品牌简介',
    keywords               varchar(255)        DEFAULT NULL COMMENT '关键词列表，各关键词使用英文的逗号分隔',
    sort                   tinyint(3) unsigned DEFAULT NULL COMMENT '排序序号',
    sales                  int(10) unsigned    DEFAULT NULL COMMENT '销量（冗余）',
    product_count          int(10) unsigned    DEFAULT NULL COMMENT '商品种类数量总和（冗余）',
    comment_count          int(10) unsigned    DEFAULT NULL COMMENT '买家评论数量总和（冗余）',
    positive_comment_count int(10) unsigned    DEFAULT NULL COMMENT '买家好评数量总和（冗余）',
    gmt_create             datetime            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified           datetime            DEFAULT NULL COMMENT '数据最后修改时间',
    PRIMARY KEY (id)
) DEFAULT CHARSET = utf8mb4 COMMENT ='品牌';


DROP TABLE IF EXISTS pms_picture;
CREATE TABLE pms_picture
(
    id           bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '数据id',
    album_id     bigint(20) unsigned  DEFAULT NULL COMMENT '相册id',
    url          varchar(255)         DEFAULT NULL COMMENT '图片url',
    description  varchar(255)         DEFAULT NULL COMMENT '图片简介',
    width        smallint(5) unsigned DEFAULT NULL COMMENT '图片宽度，单位：px',
    height       smallint(5) unsigned DEFAULT NULL COMMENT '图片高度，单位：px',
    sort         tinyint(3) unsigned  DEFAULT NULL COMMENT '排序序号',
    gmt_create   datetime             DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified datetime             DEFAULT NULL COMMENT '数据最后修改时间',
    PRIMARY KEY (id)
) DEFAULT CHARSET = utf8mb4 COMMENT ='图片';


DROP TABLE IF EXISTS pms_spu_detail;
CREATE TABLE pms_spu_detail
(
    id           bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '数据id',
    spu_id       bigint(20) unsigned DEFAULT NULL COMMENT 'SPU id',
    detail       text COMMENT 'SPU详情，应该使用HTML富文本，通常内容是若干张图片',
    gmt_create   datetime            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified datetime            DEFAULT NULL COMMENT '数据最后修改时间',
    PRIMARY KEY (id)
) DEFAULT CHARSET = utf8mb4 COMMENT ='SPU详情';


DROP TABLE IF EXISTS pms_sku;
CREATE TABLE pms_sku
(
    id                     bigint(20) unsigned NOT NULL COMMENT '数据id',
    spu_id                 bigint(20) unsigned DEFAULT NULL COMMENT 'SPU id',
    title                  varchar(255)        DEFAULT NULL COMMENT '标题',
    bar_code               varchar(255)        DEFAULT NULL COMMENT '条型码',
    attribute_template_id  bigint(20) unsigned DEFAULT NULL COMMENT '属性模版id',
    specifications         varchar(2500)       DEFAULT NULL COMMENT '全部属性，使用JSON格式表示',
    album_id               bigint(20) unsigned DEFAULT NULL COMMENT '相册id',
    pictures               varchar(1000)        DEFAULT NULL COMMENT '组图URLs，使用JSON格式表示',
    price                  decimal(10, 2)      DEFAULT NULL COMMENT '单价',
    stock                  int(10) unsigned    DEFAULT NULL COMMENT '当前库存',
    stock_threshold        int(10) unsigned    DEFAULT NULL COMMENT '库存预警阈值',
    sales                  int(10) unsigned    DEFAULT NULL COMMENT '销量（冗余）',
    comment_count          int(10) unsigned    DEFAULT NULL COMMENT '买家评论数量总和（冗余）',
    positive_comment_count int(10) unsigned    DEFAULT NULL COMMENT '买家好评数量总和（冗余）',
    sort                   tinyint(3) unsigned DEFAULT NULL COMMENT '排序序号',
    gmt_create             datetime            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified           datetime            DEFAULT NULL COMMENT '数据最后修改时间',
    PRIMARY KEY (id)
) DEFAULT CHARSET = utf8mb4 COMMENT ='SKU（Stock Keeping Unit）';



DROP TABLE IF EXISTS pms_spu;
CREATE TABLE pms_spu
(
    id                     bigint(20) unsigned NOT NULL COMMENT '数据id',
    name                   varchar(50)         DEFAULT NULL COMMENT 'SPU名称',
    type_number            varchar(50)         DEFAULT NULL COMMENT 'SPU编号',
    title                  varchar(255)        DEFAULT NULL COMMENT '标题',
    description            varchar(255)        DEFAULT NULL COMMENT 'SPU简介',
    list_price             decimal(10, 2)      DEFAULT NULL COMMENT '价格（显示在列表中）',
    stock                  int(10) unsigned    DEFAULT NULL COMMENT '当前库存（冗余）',
    stock_threshold        int(10) unsigned    DEFAULT NULL COMMENT '库存预警阈值（冗余）',
    unit                   varchar(50)         DEFAULT NULL COMMENT '计件单位',
    brand_id               bigint(20) unsigned DEFAULT NULL COMMENT '品牌id',
    brand_name             varchar(50)         DEFAULT NULL COMMENT '品牌名称（冗余）',
    attribute_template_id  bigint(20) unsigned DEFAULT NULL COMMENT '属性模版id',
    album_id               bigint(20) unsigned DEFAULT NULL COMMENT '相册id',
    pictures               varchar(1000)       DEFAULT NULL COMMENT '组图URLs，使用JSON数组表示',
    keywords               varchar(255)        DEFAULT NULL COMMENT '关键词列表，各关键词使用英文的逗号分隔',
    tags                   varchar(255)        DEFAULT NULL COMMENT '标签列表，各标签使用英文的逗号分隔，原则上最多3个',
    sales                  int(10) unsigned    DEFAULT NULL COMMENT '销量（冗余）',
    comment_count          int(10) unsigned    DEFAULT NULL COMMENT '买家评论数量总和（冗余）',
    positive_comment_count int(10) unsigned    DEFAULT NULL COMMENT '买家好评数量总和（冗余）',
    sort                   tinyint(3) unsigned DEFAULT NULL COMMENT '排序序号',
    is_deleted             tinyint(3) unsigned DEFAULT NULL COMMENT '是否标记为删除，1=已删除，0=未删除',
    is_published           tinyint(3) unsigned DEFAULT NULL COMMENT '是否上架（发布），1=已上架，0=未上架（下架）',
    is_new_arrival         tinyint(3) unsigned DEFAULT NULL COMMENT '是否新品，1=新品，0=非新品',
    is_recommend           tinyint(3) unsigned DEFAULT NULL COMMENT '是否推荐，1=推荐，0=不推荐',
    is_checked             tinyint(3) unsigned DEFAULT NULL COMMENT '是否已审核，1=已审核，0=未审核',
    check_user             varchar(50)         DEFAULT NULL COMMENT '审核人（冗余）',
    gmt_check              datetime            DEFAULT NULL COMMENT '审核通过时间（冗余）',
    gmt_create             datetime            DEFAULT NULL COMMENT '数据创建时间',
    gmt_modified           datetime            DEFAULT NULL COMMENT '数据最后修改时间',
    PRIMARY KEY (id)
) DEFAULT CHARSET = utf8mb4 COMMENT ='SPU（Standard Product Unit）';





INSERT INTO pms_album
    VALUE (1, '红旗H9的相册', '暂无', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
    (2, '红旗HS9的相册', '暂无', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
    (3, '宝马i7的相册', '暂无', 98, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
    (4, '宝马M3的相册', '暂无', 98, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
    (5, '五菱星辰的相册', '暂无', 97, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
    (6, '五零宏观PLUS的相册', '暂无', 97, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
    (7, '五菱之光的相册', '暂无', 97, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
    (8, '奥迪RS7的相册', '暂无', 96, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
    (9, '奥迪A6L的相册', '暂无', 96, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
    (10, '哈弗H6的相册', '暂无', 95, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
    (11, '哈弗大狗的相册', '暂无', 95, '2023-05-19 11:30:44', '2023-05-19 11:30:44') ;


INSERT INTO pms_brand
VALUES (1, '红旗', 'hongqi',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (2, '五菱', 'wuling',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (3, '比亚迪', 'biyadi',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (4, '大众', 'dazhong',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (5, '凯迪拉克', 'kaidilake',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (6, '传祺', 'chuanqi',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (7, '奇瑞', 'qirui',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (8, '哈弗', 'hafu',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (9, '丰田', 'fengtian',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (10, '沃尔沃', 'woerwo',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (11, '奥迪', 'aodi',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (12, '奔驰', 'benchi',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (13, '众泰', 'zhongtai',  '暂无', '暂无', 0, 0, 0,0, 0, '2023-05-19 11:30:44', '2023-05-19 11:30:44');


INSERT INTO pms_attribute
VALUES (1, 1, '车型', '轿车', '暂无', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (2, 1, '车身尺寸', '大型', '暂无', 99, '2023-05-19 11:30:44','2023-05-19 11:30:44'),
       (3, 1, '排量', '2.0T', '暂无', 99, '2023-05-19 11:30:44','2023-05-19 11:30:44'),
       (4, 1, '变速箱', '自动', '暂无', 99, '2023-05-19 11:30:44','2023-05-19 11:30:44'),
       (5, 1, '车轮驱动方式', '四驱', '暂无', 99, '2023-05-19 11:30:44','2023-05-19 11:30:44'),
       (6, 2, '车型', 'SUV', '暂无', 98, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (7, 2, '车身尺寸', '大型', '暂无', 98, '2023-05-19 11:30:44','2023-05-19 11:30:44'),
       (8, 2, '排量', '2.0T', '暂无', 98, '2023-05-19 11:30:44','2023-05-19 11:30:44'),
       (9, 2, '变速箱', '自动', '暂无', 98, '2023-05-19 11:30:44','2023-05-19 11:30:44'),
       (10, 2, '车轮驱动方式', '四驱', '暂无', 98, '2023-05-19 11:30:44','2023-05-19 11:30:44');


INSERT INTO pms_attribute_template
VALUES (1, '红旗H9的属性模板', 'HONGQIH9', '关键词1,关键词2,关键词3', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (2, '红旗HS9的属性模板', 'HONGQIHS9', '关键词1,关键词2,关键词3', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (3, '宝马i7的属性模板', 'BAOMAi7', '关键词1,关键词2,关键词3', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (4, '宝马M3的属性模板', 'BAOMAM3', '关键词1,关键词2,关键词3', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (5, '五菱星辰的属性模板', 'WULINGXINGCHEN', '关键词1,关键词2,关键词3', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (6, '五零宏观PLUS的属性模板', 'WULINGHONGGUANPLUS', '关键词1,关键词2,关键词3', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (7, '五菱之光的属性模板', 'WULINGZHIGUANG', '关键词1,关键词2,关键词3', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (8, '奥迪RS7的属性模板', 'AODIRS7', '关键词1,关键词2,关键词3', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (9, '奥迪A6L的属性模板', 'AODIA6L', '关键词1,关键词2,关键词3', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (10, '哈弗H6的属性模板', 'HAFUH6', '关键词1,关键词2,关键词3', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44'),
       (11, '哈弗大狗的属性模板', 'HAFUDAGOU', '关键词1,关键词2,关键词3', 99, '2023-05-19 11:30:44', '2023-05-19 11:30:44');