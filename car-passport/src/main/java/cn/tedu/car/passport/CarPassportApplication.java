package cn.tedu.car.passport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarPassportApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarPassportApplication.class, args);
    }

}
